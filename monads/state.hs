module State where

import Control.Monad.State

type Key = Int
type Record a = (Key, a)
type Database a = (Key, [Record a])
type Store  a = State (Database a) (Maybe a)
type StoreV a = State (Database a)

-- Initial database

emptyDb :: Database a
emptyDb = (0, [])

-- Getting record(s)

getItem :: Key -> [Record a] -> Maybe a
getItem k []     = Nothing
getItem k (x:xs) = let (k', x') = x in if k == k' then Just x' else getItem k xs

getLastR :: Store a
getLastR = state $ \db -> let (_, (_, x):_) = db in (Just x, db)

getR :: Key -> Store a
getR k = state $ \(k', s) -> (getItem k s, (k', s))

-- Adding record(s)

addR :: a -> StoreV a ()
addR x = state $ \(k, s) -> ((), (k + 1, (k + 1, x):s))

addRs :: [a] -> StoreV a ()
addRs = foldr ((>>) . addR) (state (\ db -> ((), db)))


-- Interface

makeStore :: [a] -> Store a
makeStore xs = addRs xs >> getLastR

searchByKey :: Key -> Store a -> Maybe a
searchByKey k s = evalState store emptyDb
  where store = s >> getR k

seeRecords :: Store a -> [Record a]
seeRecords s = snd $ execState s emptyDb

-- testing

test :: Store String
test = do
  addRs ["Hello", "Bye", "Anthony"]
  addR "09"
  getR 3