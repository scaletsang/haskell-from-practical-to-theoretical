module Writer where

import Control.Monad.Writer ( MonadWriter(writer), Writer )

add :: Int -> Int -> Int
add x y = x + y

startW :: Int -> Writer [Int] Int
startW x = writer (x, [x])

addW :: Int -> Int -> Writer [Int] Int
addW x y = writer (x + y, [x])

addList :: [Int] -> Writer [Int] Int
addList []     = writer (0, [])
addList (x:xs) = startW x >>= addList' xs

addList' :: [Int] -> Int -> Writer [Int] Int
addList' []     y = return y
addList' (x:xs) y = addW x y >>= addList' xs
