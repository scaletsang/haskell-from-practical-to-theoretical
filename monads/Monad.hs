{-# LANGUAGE InstanceSigs, TupleSections, NoImplicitPrelude #-}
module Monad where
import qualified Prelude as P

class Semigroup s where
  (<>) :: s -> s -> s

class Semigroup m => Monoid m where
  mempty :: m
  mconcat :: [m] -> m

class Functor f where
  fmap :: (a -> b) -> f a -> f b

class Functor f => Applicative f where
  pure  :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b

class Applicative m => Monad m where
  return :: a -> m a
  (>>=)  :: m a -> (a -> m b) -> m b

--------------------------------
-- Maybe Monad implementation --
--------------------------------

data Maybe a = Nothing | Just a

instance Functor Maybe where
  fmap :: (a -> b) -> Maybe a -> Maybe b
  fmap g Nothing  = Nothing
  fmap g (Just a) = Just (g a)

instance Applicative Maybe where
  pure :: a -> Maybe a
  pure a = Just a

  (<*>) :: Maybe (a -> b) -> Maybe a -> Maybe b
  -- example: pure (a -> b -> c) <*> Maybe a <*> Maybe b
  -- pure (a -> b -> c) :: Maybe (a -> b -> c)
  -- pure (a -> b -> c) <*> Maybe a :: Maybe (a -> (b -> c)) -> Maybe a -> Maybe (b -> c)
  -- pure (a -> b -> c) <*> Maybe a <*> Maybe b :: Maybe (b -> c) -> Maybe b -> Maybe c
  Nothing <*> a = Nothing
  Just g <*> a  = fmap g a

instance Monad Maybe where
  return :: a -> Maybe a
  return = pure

  (>>=) :: Maybe a -> (a -> Maybe b) -> Maybe b
  Nothing >>= g = Nothing
  Just a >>= g  = g a

-------------------------------
-- List Monad implementation --
-------------------------------

instance Functor [] where
  fmap :: (a -> b) -> [a] -> [b]
  fmap = map

instance Applicative [] where
  pure :: a -> [a]
  pure a = [a]

  (<*>) :: [a -> b] -> [a] -> [b]
  (<*>) [] xs = []
  (<*>) (g:gs) xs = fmap g xs ++ (gs <*> xs)

instance Monad [] where
  return :: a -> [a]
  return = pure

  (>>=) :: [a] -> (a -> [b]) -> [b]
  (>>=) [] g = []
  (>>=) (x:xs) g = g x ++ (xs >>= g)

-------------------------------
-- Tree Monad implementation --
-------------------------------
data Tree a = Leaf a | Node (Tree a) (Tree a)

instance Functor Tree where
  fmap :: (a -> b) -> Tree a -> Tree b
  fmap g (Leaf a)   = Leaf (g a)
  fmap g (Node l r) = Node l' r'
    where l' = fmap g l
          r' = fmap g r

instance Applicative Tree where
  pure :: a -> Tree a
  pure a = Leaf a

  (<*>) :: Tree (a -> b) -> Tree a -> Tree b
  Leaf g <*> a     = fmap g a
  Node lg rg <*> a = Node lg' rg'
    where lg' = lg <*> a
          rg' = rg <*> a

instance Monad Tree where
  return :: a -> Tree a
  return = pure

  (>>=) :: Tree a -> (a -> Tree b) -> Tree b
  Leaf a >>= g   = g a
  Node l r >>= g = Node (l >>= g) (r >>= g)

-----------------------------------
-- identity Monad implementation --
-----------------------------------
newtype Identity a = Identity {runIdentity :: a }

instance Functor Identity where
  fmap :: (a -> b) -> Identity a -> Identity b
  fmap g (Identity a) = Identity (g a)

instance Applicative Identity where
  pure :: a -> Identity a
  pure a = Identity a

  (<*>) :: Identity (a -> b) -> Identity a -> Identity b
  Identity g <*> Identity a = Identity (g a)

instance Monad Identity where
  return :: a -> Identity a
  return = pure

  (>>=) :: Identity a -> (a -> Identity b) -> Identity b
  Identity a >>= g = g a

---------------------------------
-- Writer Monad implementation --
---------------------------------

newtype Writer w a = Writer {runWriter :: (a, w)}

instance Functor (Writer w) where
  fmap :: (a -> b) -> Writer w a -> Writer w b
  fmap g mw = Writer $ let (a, w) = runWriter mw in (g a, w)

instance Monoid w => Applicative (Writer w) where
  pure :: a -> Writer w a
  pure a = Writer (a, mempty)
  (<*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
  (<*>) wg mw = Writer $ let (g, w)  = runWriter wg
                             (a, w') = runWriter (fmap g mw)
                         in  (a, w <> w')

instance Monoid w => Monad (Writer w) where
  return :: a -> Writer w a
  return = pure
  (>>=) :: Writer w a -> (a -> Writer w b) -> Writer w b
  (>>=) mw g = Writer $ let (a, w)   = runWriter mw
                            (a', w') = runWriter (g a)
                        in  (a', w <> w')

--------------------------------
-- State Monad implementation --
--------------------------------
newtype State s a = State {runState :: s -> (a, s)}

instance Functor (State s) where
  fmap :: (a -> b) -> State s a -> State s b
  -- fmap :: (a -> b) -> State (s -> (a, s)) -> State (s -> (b, s))
  fmap g ms = State $ \s -> let (a, s') = runState ms s in (g a, s')

instance Applicative (State s) where
  pure :: a -> State s a
  pure a = State (a,)
  (<*>) :: State s (a -> b) -> State s a -> State s b
  (<*>) gs ms = State $ \s -> let (g, s')  = runState gs s 
                                  (a, s'') = runState ms s'
                              in  (g a, s'')

instance Monad (State s) where
  return :: a -> State s a
  return = pure
  (>>=) :: State s a -> (a -> State s b) -> State s b
  (>>=) ms g = State $ \s -> let (a, s') = runState ms s in runState (g a) s'

-------------------------------
--      Helper functions     --
-------------------------------

($) :: (a -> b) -> a -> b
a $ b = a b

map :: (a -> b) -> [a] -> [b]
map _ [] = []
map g (x:xs) = g x : map g xs

(++) :: [a] -> [a] -> [a]
[] ++ ys = ys
(x:xs) ++ ys = x : (xs ++ ys)