# Haskell from practical to theoretical
This is a course for anyone who sees any degree of beauty in Haskell and want to explore its depth. 

## Basic Haskell
[Learn you a Haskell for Great Good!](http://learnyouahaskell.com/chapters)  

## The Theoratical: Category theory and typeclass
[Category Theory for Programmers Book](https://bartoszmilewski.com/2014/10/28/category-theory-for-programmers-the-preface/)  
[Video Lecture of this Book](https://youtube.com/playlist?list=PLbgaMIhjbmEnaH_LTkxLI7FMa2HsnawM_)  

## The Practical: Building Systems
[Parallelism and Concurrency Book](https://www.oreilly.com/library/view/parallel-and-concurrent/9781449335939/)  

## Monads
IO  
Writer  
Reader  
State  
Monad Transformer (Monad inside monad)  

## Array
Arrays  
Haskell Vector  

## Lists
Double linked list  
Stack  
Queres  
Hash table  

## Graphs
Graphs  
Dijkstra’s algorithm  
Binary search trees  
Binary search  
AVL trees  
Tries  

## Sorting
Bubble sort  
Selection sort  
Insertion sort  
Heap sort  
Quick sort  
Merge sort  

## Other concepts
Big O notation  
foldr vs foldl  
+ [Lazy evaluation with foldl or foldr](https://stackoverflow.com/questions/12296694/the-performance-of-with-lazy-evaluation)
Dynamic programming (memorization)  
Lazy computation  

## About me and motivation
The first programming language I encountered is Lisp (chicken lisp) and then Haskell. I love Haskell for its elegance that allows me to write functions that are not only well organized but idiomatic to how a normal human can compose functions. I tried to build complex web application with Ruby and Javascipt and I feel absolute pain to think about composition and code organization with these two language. And I decided to take a deeper dive into Haskell, and not just write Haskell codes causually (although I'm still doing this causually), but learn Haskell to its absolute depth systematically. This is a repo of me designing my own course of Haskell and then going through the course on the fly. I am my own professor.
