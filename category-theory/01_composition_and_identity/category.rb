def id f
	f
end

def compose f, g
	lambda {|x| f.call(g.call(x))}
end

def	testing_func x
	[x, x, x]
end

def	prog
	a = compose method(:testing_func), method(:id)
	b = compose method(:id), method(:testing_func)
	v = 7
	puts a.call(v) == testing_func(v)
	puts b.call(v) == testing_func(v)
end

prog