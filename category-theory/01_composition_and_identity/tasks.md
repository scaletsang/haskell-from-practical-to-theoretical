# What can be a category in reality? What is the implication of a category?

## Tasks for this chapter:
1. Implement, as best as you can, the identity function in your favourite programming language (or the second favourite, if your favourite language happens to be Haskell).
2. Implement the composition function in your favourite programming language. It takes two functions as arguments and return a function that is their composition.
3. Write a program that tries to test that your composition function respects identity.
4. Is the world-wide web a category in any sense? Are links morphisms?
5. Is Facebook a category, with people as objects and friendships as morphisms?
6. When is a directed graph a category?

Category is a collection of relations between objects. The differentiation of the objects is that they have different underlying structures within them. If links are relations between the objects, then what are the objects in the category of world-wide web? Websites? Communities? Servers?

A function is a group of arrows that collapse one or more objects to one object. It is therefore a function, more accurately non-injective functions contributes to abstraction. In terms of category theory, it is called monomorphism.
