# Composition and Identity

Category as objects with arrows pointing between them. How the objects are implemented is collapsed into a point and therefore unobservable. The only observable entity at hands are the arrows point between the objects. An arrow represents a morphism that effectively transform an object to another object. In the sense of sets as objects, an arrow represents a mapping that maps a sets of values to another sets of values. In the sense of types as objects, an arrow represents a function that maps a type of value to another type of values. 

This chapter talks about 2 properties of compositions in any category:
1. Associativity: `h . (f . g) = (h . f) . g = h . f . g`
   In other words, if there exsits f . g, then there must be another morphism let's say k = f .g
2. For every category, there must be at least one identity morphism that maps itself to itself:
   `f . id = f, id . f = f`
