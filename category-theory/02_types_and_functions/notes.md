# Types and Functions

## Types

Static vs. Dynamic typing **(Type check when?)**
+ Static typing allows compiled-time type checking
+ Dynamic typing allows run-time type checking

Strong vs. Weak typing **(How strict is the type checks?)**
It is about how strong the rules for type checking is

Units testing and type checking,
testing is for understanding the black box, and sometimes maybe probablistic weird combinations of unexpected inputs.
Type check is a proof for the isomorphism between types.

Types as sets

Special value of any types, a hack - the bottom (_|_)

