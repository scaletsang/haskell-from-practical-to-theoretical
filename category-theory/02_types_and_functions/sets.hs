-- empty set - Void

newtype Void = Void Void

absurd :: Void -> a
absurd x = case x of { }

-- singleton set - ()

unit :: a -> ()
unit _ = ()

one :: () -> Int
one () = 1

-- 2 element set - Boolean
-- predicate :: a -> Bool